package com.example.duypc.youtobe.object;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.duypc.youtobe.adapter.NewVideoAdapter;
import com.example.duypc.youtobe.library.CustomRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Xnono_S2 on 3/5/2016.
 */
public class NewVideo implements Serializable{
    public String title;
    public String url;
    public String channelTitle;
    public String duration;
    public String videoId;
    public String viewCount;
    public String playlistId="";
    public String publishedAt;
    public String timeUpLoad;

    public String getTimeUpLoad() {
        return timeUpLoad;
    }

    public void setTimeUpLoad(String timeUpLoad) {
        this.timeUpLoad = timeUpLoad;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    public String getViewCount() {
        return viewCount;
    }

    public void setViewCount(String viewCount) {
        this.viewCount = viewCount;
    }

    public NewVideo(String url, String channelTitle, String duration, String title, String videoId) {
        this.url = url;
        this.channelTitle = channelTitle;
        this.duration = duration;
        this.title = title;
        this.videoId = videoId;
    }
    public NewVideo(String url, String viewCount, String duration, String title, String videoId,int a) {
        this.url = url;
        this.viewCount = viewCount;
        this.duration = duration;
        this.title = title;
        this.videoId = videoId;
    }

    public NewVideo(String viewCount,  String title,String url,String videoId) {
        this.videoId =videoId;
        this.viewCount = viewCount;
        this.url = url;
        this.title = title;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public NewVideo() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public static NewVideo parserJson(JSONObject jsonObject) throws JSONException {
        NewVideo i = new NewVideo();
        i.url = jsonObject.getJSONObject("thumbnails").getJSONObject("high").getString("url");
        i.title = jsonObject.getString("title");
        i.channelTitle = jsonObject.getString("channelTitle");
        i.publishedAt = jsonObject.getString("publishedAt");
        return  i;
    }

    public static String convertViewCount(String s){
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        String numberAsString = numberFormat.format(Integer.parseInt(s));
        return numberAsString;
    }


    public static String convertDuration(String youtubeDuration){
        Calendar c = new GregorianCalendar();
        try {
            DateFormat df = new SimpleDateFormat("'PT'mm'M'ss'S'");
            Date d = df.parse(youtubeDuration);
            c.setTime(d);
        } catch (ParseException e) {
            try {
                DateFormat df = new SimpleDateFormat("'PT'hh'H'mm'M'ss'S'");
                Date d = df.parse(youtubeDuration);
                c.setTime(d);
            } catch (ParseException e1) {
                try {
                    DateFormat df = new SimpleDateFormat("'PT'ss'S'");
                    Date d = df.parse(youtubeDuration);
                    c.setTime(d);
                } catch (ParseException e2) {
                }
            }
        }
        c.setTimeZone(TimeZone.getDefault());

        String time = "";
        if ( c.get(Calendar.HOUR) > 0 ) {
            if ( String.valueOf(c.get(Calendar.HOUR)).length() == 1 ) {
                time += "0" + c.get(Calendar.HOUR);
            }
            else {
                time += c.get(Calendar.HOUR);
            }
            time += ":";
        }
        if ( String.valueOf(c.get(Calendar.MINUTE)).length() == 1 ) {
            time += "0" + c.get(Calendar.MINUTE);
        }
        else {
            time += c.get(Calendar.MINUTE);
        }
        time += ":";
        if ( String.valueOf(c.get(Calendar.SECOND)).length() == 1 ) {
            time += "0" + c.get(Calendar.SECOND);
        }
        else {
            time += c.get(Calendar.SECOND);
        }
        return time ;
    }
}

