package com.example.duypc.youtobe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.duypc.youtobe.adapter.NewVideoAdapter;
import com.example.duypc.youtobe.library.CustomRequest;
import com.example.duypc.youtobe.object.NewVideo;
import com.example.duypc.youtobe.object.Playlist;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Admin on 06/07/16.
 */
public class CatelogyActivity extends Activity implements View.OnClickListener{
    private ArrayList<NewVideo>newVideos;
    private Playlist playlist;
    private NewVideoAdapter newVideoAdapter;

    private ListView lv_playlist_category;
    private ImageView iv_btn_back;
    private TextView tv_title,tv_number;
    private ProgressBar pbHeaderProgressListview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        lv_playlist_category=(ListView)findViewById(R.id.lv_playlist_category);
        iv_btn_back=(ImageView)findViewById(R.id.iv_btn_back);
        tv_title=(TextView)findViewById(R.id.tv_title);
        pbHeaderProgressListview=(ProgressBar)findViewById(R.id.pbHeaderProgressListview);
        tv_number=(TextView)findViewById(R.id.tv_number);

        playlist= (Playlist) getIntent().getSerializableExtra("playlist");
        tv_title.setText(playlist.getTitle());

        setNewVideos();
        lv_playlist_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(CatelogyActivity.this, VideoYoutobe.class);
                intent.putExtra(VideoYoutobe.VIDEO_ID, newVideos.get(position).getVideoId());
                intent.putExtra(VideoYoutobe.TITLE, newVideos.get(position).getTitle());
                intent.putExtra(VideoYoutobe.VIEWCOUNT, newVideos.get(position).getViewCount());
                intent.putExtra(VideoYoutobe.URL, newVideos.get(position).getUrl());
                intent.putExtra(VideoYoutobe.PLAYLISTID, newVideos.get(position).getPlaylistId());
                intent.putExtra(VideoYoutobe.TIME, newVideos.get(position).getDuration());
                intent.putExtra(VideoYoutobe.ACTION, "1");
                CatelogyActivity.this.startActivity(intent);
            }
        });
        iv_btn_back.setOnClickListener(this);
    }
    private void setNewVideos(){
        pbHeaderProgressListview.setVisibility(View.VISIBLE);
        newVideos=new ArrayList<>();
        String url = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&playlistId="+playlist.getId()+"&key=AIzaSyCt0QdTz8JJunSgh3uqbiKB71EdAwITXLU&maxResults=50";
        final Activity activity=this;
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                newVideoAdapter = new NewVideoAdapter(newVideos,activity);
                lv_playlist_category.setAdapter(newVideoAdapter);
                try {
                    JSONArray jsonArray = response.getJSONArray("items");
                    for(int i=0;i<jsonArray.length();i++){
                        getNewVideo(jsonArray.getJSONObject(i), activity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "tag that bai " + error.getMessage());
                pbHeaderProgressListview.setVisibility(View.GONE);
            }
        });
        jsObjRequest.setShouldCache(false);
        requestQueue.add(jsObjRequest);
    }
    private void getNewVideo(final JSONObject jsonObject1, final Activity activity) throws JSONException {
        final JSONObject jsonObject2 = jsonObject1.getJSONObject("snippet");
        final JSONObject jsonObject3 = jsonObject2.getJSONObject("resourceId");
        final NewVideo i = new NewVideo();
        i.setPublishedAt(jsonObject2.getString("publishedAt"));
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        String url_video = "https://www.googleapis.com/youtube/v3/videos?id="+ jsonObject3.getString("videoId")+"&key=AIzaSyCt0QdTz8JJunSgh3uqbiKB71EdAwITXLU&part=snippet,contentDetails,statistics,status";
        i.url = jsonObject2.getJSONObject("thumbnails").getJSONObject("high").getString("url");
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url_video, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("videos", response.toString());
                    i.title = jsonObject2.getString("title");
                    i.playlistId = jsonObject2.getString("playlistId");
                    i.channelTitle = jsonObject2.getString("channelTitle");
                    i.duration = NewVideo.convertDuration(response.getJSONArray("items").getJSONObject(0).getJSONObject("contentDetails").getString("duration"));
                    i.viewCount = NewVideo.convertViewCount(response.getJSONArray("items").getJSONObject(0).getJSONObject("statistics").getString("viewCount").toString());
                    i.setVideoId(jsonObject3.getString("videoId"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                newVideos.add(i);
                newVideoAdapter.Update(i);
                if(pbHeaderProgressListview.getVisibility()==View.VISIBLE){
                    pbHeaderProgressListview.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(pbHeaderProgressListview.getVisibility()==View.VISIBLE){
                    pbHeaderProgressListview.setVisibility(View.GONE);
                }
                Log.e("error", "tag that bai " + error.getMessage());
            }
        });
        jsObjRequest.setShouldCache(false);
        requestQueue.add(jsObjRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_btn_back:
                finish();
                break;
        }
    }
}
